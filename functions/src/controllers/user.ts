import { Request, Response } from "express"
import { User, UserDocument } from "../models/user"
// import * as moment from 'moment-timezone'
import { toMoment, toTimestamp } from "../utils/date"
import { authFb } from "../main"
import { Crypto } from "../utils/crypto"

export class UserController {
    async route_postJSON(req: Request, res: Response) { //teste com auth
        const doc = req.body //email e password
        if (!doc.email && !doc.password) return res.status(400).json({"response": "email and password are necessary"})
        doc.password = await Crypto.decrypt(doc.password)
        let userAuth
        try {
            userAuth = await authFb.createUserWithEmailAndPassword(doc.email, doc.password)
        } catch (error) {
            return res.status(422).json({"response": error.code})
        }
        const user = new User({
            id: userAuth.user.uid,
            name: doc.name,
            email: doc.email
        })
        if(!await user.create()) return res.status(422).json({"response": "error on creating user"})
        return res.json({
            "user": toMoment(user)
        })
    }

    async route_getLoginJSON(req: Request, res: Response) { //teste com auth
        const doc = req.body //email e password
        doc.password = await Crypto.decrypt(doc.password)
        const userAuth = await authFb.signInWithEmailAndPassword(doc.email, doc.password)
        return res.json({
            "user": toMoment(userAuth.user)
        })
    }

    async route_getJSON(req: Request, res: Response) {
        const userId = req.params.userId
        // if (!userId) return res.status(400).send({"response": "userId is necessary"})
        const user = new User()
        if (!await user.load(userId)) return res.status(400).json({"response": "user not found", userId})
        user.fields.id = user.id
        return res.json({
            "user": toMoment(user.fields)
        })
    }

    async route_deleteJSON(req: Request, res: Response){
        try {
            await new User().delete(req.params.userId)
            return res.json({
                "response": "user successfully deleted"
            })
        } catch (error) {
            console.error(error)
            return res.json(error)
        }
    }

    async route_putJSON(req: Request, res: Response) {
        const user = new User()
        const userId = req.params.userId
        if(!await user.load(userId)) return res.status(400).json({"response": "user not found", userId})
        Object.assign(user.fields, toTimestamp(req.body))
        if (!await user.save()) return res.status(422).json({"response": "error on update user"})
        user.fields.id = user.id
        return res.json({
            "user": toMoment(user.fields)
        })
    }
}

export const instance = new UserController()