import * as admin from "firebase-admin"
import * as firebase from "firebase"

admin.initializeApp();
export const db = admin.firestore();
db.settings({timestampsInSnapshots: true});
// export const auth = admin.auth();
firebase.initializeApp({
    apiKey: "AIzaSyA_PAO56i6RFQwJN7ftFliQANECkAuGLEI",
    authDomain: "process-manager-study.firebaseapp.com",
    databaseURL: "https://process-manager-study.firebaseio.com",
    projectId: "process-manager-study",
    storageBucket: "process-manager-study.appspot.com",
    messagingSenderId: "113364796344",
    appId: "1:113364796344:web:46bd2cc31a1e13b489454d"
})
export const authFb = firebase.auth()

