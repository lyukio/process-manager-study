import {Base, Document} from "./base"

export type UserDocument = Document & {
    id?: string
    name?: string
    email: string
    // password: string
}

export class User extends Base {

    fields: UserDocument;
    constructor(fields: any = {}) {
        super("users");

        this.fields = {
            id: fields.id,
            name: fields.name,
            email: fields.email || "",
            // password: fields.password || ""
        }
        for (const field in this.fields)
            if (typeof this.fields[field] === "undefined") delete this.fields[field]
    }

    async create() {
        this.id = this.fields.id;
        return await super.save();
    }

    async save() {
        return await super.save();
    }
}