import SimpleCrypto from "simple-crypto-js"

export abstract class Crypto {
    private static secret = "Huddle!2018#" //colocar numa variável de ambiente (falar pro Fernando amanhã)
    private static simpleCrypto = new SimpleCrypto(Crypto.secret)

    static encrypt(decryptedData: string) {
        return Crypto.simpleCrypto.encrypt(decryptedData)
    }

    static decrypt(encryptedData: string) {
        return Crypto.simpleCrypto.decrypt(encryptedData)
    }

    static compare(encryptedData, decryptedData) {
        return Crypto.decrypt(encryptedData) === decryptedData
    }
}