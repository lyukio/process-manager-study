import * as jwt from "jsonwebtoken"

export type TokenPayload = {
    [key: string]: any
    userId: string
}
export abstract class Token { //falar que eu lembrei das classes abstratas ontem
    private static secret = "secretKey"
    static generate(payload: TokenPayload, expiresIn = "24h") {
        const token = jwt.sign(payload, this.secret, {expiresIn})
        return token
    }

    static verify(token: string) {
        try {
            return jwt.verify(token, this.secret)
        } catch(err) {
            return null
        }
    }
}