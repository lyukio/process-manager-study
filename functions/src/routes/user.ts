import {instance as controller} from '../controllers/user'
import * as express from "express"

const router = express.Router()
router.post('/', controller.route_postJSON.bind(controller))
router.get('/:userId', controller.route_getJSON.bind(controller))
router.delete('/:userId', controller.route_deleteJSON.bind(controller))
router.put('/:userId', controller.route_putJSON.bind(controller))

export const userRouter = router